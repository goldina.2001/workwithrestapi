package com.goldina.workwithrestapi

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL="https://jsonplaceholder.typicode.com/"
class MainActivity : AppCompatActivity() {
    private lateinit var myAdapter: MyAdapter
    private lateinit var linearLayoutManager:LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerViewUsers.setHasFixedSize(true)
        linearLayoutManager= LinearLayoutManager(this)
        recyclerViewUsers.layoutManager=linearLayoutManager
        getMyData()
        with(ItemClickSupport.addTo(recyclerViewUsers)) {
            setOnItemClickListener { _, position, _ ->
                openEditPost(position)}
        }
    }

    private fun openEditPost(position:Int) {
        val user = myAdapter.userList[position]
        val intent = Intent(this, EditPostActivity::class.java)
        intent.putExtra("userId", user.userId)
        intent.putExtra("id", user.id)
        intent.putExtra("title", user.title)
        intent.putExtra("body", user.body)
        startActivity(intent)
    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)
        val retrofitData = retrofitBuilder.getData()
        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<List<MyDataItem>?>,
                response: Response<List<MyDataItem>?>,
            ) {
                val responseBody = response.body()!!
                myAdapter = MyAdapter(baseContext,responseBody)
                myAdapter.notifyDataSetChanged()
                recyclerViewUsers.adapter = myAdapter

            }

            override fun onFailure(call: Call<List<MyDataItem>?>, t: Throwable) {
                d("MainActivity", "onFailure: ${t.message}")
            }
        })
    }
}