package com.goldina.workwithrestapi

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiInterface {
    @GET("posts")
    fun getData():Call<List<MyDataItem>>

    @PUT("posts/{id}")
     fun updatePost(
        @Path("id") id:Int,
        @Body body: MyDataItem): Call<MyDataItem>

}