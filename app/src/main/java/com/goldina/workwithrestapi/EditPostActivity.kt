package com.goldina.workwithrestapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_edit_post.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.properties.Delegates


class EditPostActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_post)
        val id =  intent.extras!!["id"].toString().toInt()
        etUserId.setText(intent.extras!!["userId"].toString())
        etBody.setText(intent.extras!!["body"].toString())
        etTitle.setText(intent.extras!!["title"].toString())
        textViewID.text =id.toString()

        buttonSend.setOnClickListener {
            updateData(id)
            finish()
        }
    }
    private fun updateData(id:Int){
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)
        val newData = MyDataItem(etBody.text.toString()
            ,id
            ,etTitle.text.toString()
            ,etUserId.text.toString().toInt())
        val call = retrofitBuilder.updatePost(id,newData)
        call.enqueue(object : Callback<MyDataItem?> {
            override fun onResponse(call: Call<MyDataItem?>, response: Response<MyDataItem?>) {
                Toast.makeText(this@EditPostActivity
                    , "Data updated.Code"+response.code()
                    , Toast.LENGTH_SHORT).show()
            }
            override fun onFailure(call: Call<MyDataItem?>, t: Throwable) {
                Log.d("EditPostActivity", "onFailure: ${t.message}")
            }
        })
    }
}